$(document).ready(function(){
    //username validation
    var validusername = false;
    var validfirstname = false;
    var validlastname = false;
    var validemail = false;
    var validpass = false;
    function validateUsername() {
        let usernamevalue = $("#username").val();
        if (usernamevalue.length == "") {
            $("#username-error").css('display' , 'block');
        } else if (usernamevalue.length < 3 || usernamevalue.length > 10) {
            $("#username-error").css('display' , 'block');
            $("#username-error").html("length of username must be between 3 and 10");
        } else {
            $("#username-error").hide();
            validusername = true;
        }
    }
    //firstname validation
    function validateFirstname() {
        let firstnamevalue = $("#firstname").val();
        if (firstnamevalue.length == "") {
            $("#fname-error").css('display' , 'block');
        } else if (firstnamevalue.length < 2) {
            $("#fname-error").css('display' , 'block');
            $("#fname-error").html("length of username must be above 2");
        }  else {
            $("#fname-error").hide();
            validfirstname = true;
        }
    }
    //lastname validation
    function validateSecondname() {
        let lastnamevalue = $("#lastname").val();
        if (lastnamevalue.length == "") {
            $("#lname-error").css('display' , 'block');
        } else if (lastnamevalue.length < 2) {
            $("#lname-error").css('display' , 'block');
            $("#lname-error").html("length of username must be above 2");
        } else {
            $("#lname-error").hide();
            validlastname = true;
        }
    }
    //email validation
    let emailregex = /^([_\-\.0-9a-zA-Z]+)@([_\-\.0-9a-zA-Z]+)\.([a-zA-Z]){2,7}$/;
    function validateemail() {
        let emailvalue = $("#email").val();
        if(emailvalue.length == "") {
            $("#email-error").css('display' , 'block');
        } else if (!emailregex.test(emailvalue)) {
            $("#email-error").html("Please enter a valid emailid").css('display' , 'block');
        } else {
            $("#email-error").hide();
            validemail = true;
        }
    }
    //password validation
    let passregex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    function validatePassword() {
        let passwordvalue = $("#password").val();
        if(passwordvalue.length == "") {
            $("#password-error").css('display' , 'block');
        } else if (!passregex.test(passwordvalue)) {
            $("#password-error").html("Password should contain 1 Uppercase 1 Lowercase 1 Number 1 special charcter and length should be 8 ").css('display' , 'block');
        } else {
            $("#password-error").hide();
            validpass = true;
        }
    }

    $("#submitbtn").click(function(){
        validateUsername();
        validateFirstname();
        validateSecondname();
        validateemail();
        validatePassword();
        if (validusername && validfirstname && validlastname && validemail && validpass) {
            $(".signin_form").css('display' , 'flex');
            $(".signup_form").hide();
        }
    });
    $("#submitbtn2").click(function(){
        validateUsername();
        validatePassword();
    });
});